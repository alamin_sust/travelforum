<%-- 
    Document   : home
    Created on : Apr 30, 2018, 12:09:29 AM
    Author     : md_al
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.travelforum.connection.Database"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Maxim - Modern One Page Bootstrap Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="resources/css/bootstrap-responsive.css" rel="stylesheet">
        <link href="resources/css/style.css" rel="stylesheet">
        <link href="resources/color/default.css" rel="stylesheet">
        <link rel="shortcut icon" href="resources/img/favicon.ico">
        <!-- =======================================================
    Theme Name: Maxim
    Theme URL: https://bootstrapmade.com/maxim-free-onepage-bootstrap-theme/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
        ======================================================= -->
    </head>

    <body>
        <%
            if (session.getAttribute("username") == null || session.getAttribute("username").toString().equals("")) {
                response.sendRedirect("loginRegister.jsp");
            }

            Database db = new Database();
            db.connect();

            try {

                Statement st1 = db.connection.createStatement();
                String q1 = "";
                ResultSet rs1;

                if (request.getParameter("title") != null && !request.getParameter("title").equals("")) {

                    Statement st0 = db.connection.createStatement();
                    String q0 = "select max(id)+1 as mx from post";
                    ResultSet rs0 = st0.executeQuery(q0);
                    rs0.next();

                    q1 = "insert into post (id,title,sub_title,date,content,user_id) values(" + rs0.getString("mx") + ",'" + request.getParameter("title")
                            + "','" + request.getParameter("subTitle") + "','" + request.getParameter("date") + "','" + request.getParameter("content") + "'," + session.getAttribute("id") + ")";
                    st1.executeUpdate(q1);
                    session.setAttribute("successMsg", "Posted Successfully!");
                }

        %>


        <!-- navbar -->
        <div class="navbar-wrapper">
            <div class="navbar navbar-inverse navbar-fixed-top">
                <div class="navbar-inner">
                    <div class="container">
                        <!-- Responsive navbar -->
                        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                        </a>
                        <h1 class="brand"><a href="home.jsp">Travel Forum</a></h1>
                        <!-- navigation -->
                        <nav class="pull-right nav-collapse collapse">
                            <ul id="menu-main" class="nav">
                                <li><a title="team" href="home.jsp">Home</a></li>
                                <li><a title="services" href="posts.jsp">All Posts</a></li>
                                <li><a title="works" href="addPost.jsp">Add New Post</a></li>
                                    <%if (session.getAttribute("username") != null && !session.getAttribute("username").toString().equals("")) {%>
                                <li><a title="blog" href="loginRegister.jsp">Logout(<%=session.getAttribute("username").toString()%>)</a></li>
                                    <%} else {%>
                                <li><a title="blog" href="loginRegister.jsp">Login/Register</a></li>
                                    <%}%>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- section: contact -->
        <section id="contact" class="section green">
            <div class="container">
                <%if (session.getAttribute("successMsg") != null && !session.getAttribute("successMsg").equals("")) {%>
                <div class="alert alert-success">
                    <%=session.getAttribute("successMsg")%>
                </div><%
                    session.setAttribute("successMsg", null);
                }%>
                <h4>Add New Travel Post</h4>

                <div class="blankdivider30">
                </div>
                <div class="row">
                    <div class="span12">
                        <div class="cform" id="contact-form">
                            <form action="addPost.jsp" method="post" class="contactForm">
                                <div class="row">
                                    <div class="span6">
                                        <div class="field your-name form-group">
                                            <input type="text" name="title" class="form-control" placeholder="Post Title"  required/>
                                           
                                        </div>
                                        <div class="field your-email form-group">
                                            <input type="text" name="subTitle" class="form-control" placeholder="Post Sub Title" required/>
                                            
                                        </div>
                                        <div class="field subject form-group">
                                            <input type="date" class="form-control" name="date" placeholder="Date of travel" required/>
                                            
                                        </div>
                                    </div>
                                    <div class="span6">
                                        <div class="field message form-group">
                                            <textarea class="form-control" name="content" rows="5" placeholder="Main Content" required></textarea>
                                            
                                        </div>
                                        <input type="submit" value="Post" class="btn btn-theme pull-left"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- ./span12 -->
                </div>
            </div>
        </section>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="span6 offset3">
                        <p class="copyright">
                            &copy; Maxim Theme. All rights reserved.
                        <div class="credits">
                            <!--
All the links in the footer should remain intact.
You can delete the links only if you purchased the pro version.
Licensing information: https://bootstrapmade.com/license/
Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Maxim
                            -->
                            <a href="https://bootstrapmade.com/">Free Bootstrap Templates</a> by BootstrapMade.com
                        </div>
                        </p>
                    </div>
                </div>
            </div>
            <!-- ./container -->
        </footer>
        <a href="#" class="scrollup"><i class="icon-angle-up icon-square icon-bgdark icon-2x"></i></a>
        <script src="resources/js/jquery.js"></script>
        <script src="resources/js/jquery.scrollTo.js"></script>
        <script src="resources/js/jquery.nav.js"></script>
        <script src="resources/js/jquery.localScroll.js"></script>
        <script src="resources/js/bootstrap.js"></script>
        <script src="resources/js/jquery.prettyPhoto.js"></script>
        <script src="resources/js/isotope.js"></script>
        <script src="resources/js/jquery.flexslider.js"></script>
        <script src="resources/js/inview.js"></script>
        <script src="resources/js/animate.js"></script>
        <script src="resources/js/custom.js"></script>
        <script src="resources/contactform/contactform.js"></script>
        <%
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                db.close();
            }
        %>
    </body>

</html>
