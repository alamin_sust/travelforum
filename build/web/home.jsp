<%-- 
    Document   : home
    Created on : Apr 30, 2018, 12:09:29 AM
    Author     : md_al
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.travelforum.connection.Database"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Maxim - Modern One Page Bootstrap Template</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link href="resources/css/bootstrap-responsive.css" rel="stylesheet">
	<link href="resources/css/style.css" rel="stylesheet">
	<link href="resources/color/default.css" rel="stylesheet">
	<link rel="shortcut icon" href="resources/img/favicon.ico">
	<!-- =======================================================
    Theme Name: Maxim
    Theme URL: https://bootstrapmade.com/maxim-free-onepage-bootstrap-theme/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
	======================================================= -->
</head>

<body>
    <%
    
    Database db = new Database();
            db.connect();

            try {

                Statement st1 = db.connection.createStatement();
                String q1 = "select * from post where id>0";
                ResultSet rs1 = st1.executeQuery(q1);
                
    %>
	<!-- navbar -->
	<div class="navbar-wrapper">
		<div class="navbar navbar-inverse navbar-fixed-top">
                        
			<div class="navbar-inner">
				<div class="container">
					<!-- Responsive navbar -->
					<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
				</a>
                                        
					<h1 class="brand"><a href="home.jsp">Travel Forum</a></h1>
					<!-- navigation -->
					<nav class="pull-right nav-collapse collapse">
						<ul id="menu-main" class="nav">
							<li><a title="team" href="home.jsp">Home</a></li>
							<li><a title="services" href="posts.jsp">All Posts</a></li>
							<li><a title="works" href="addPost.jsp">Add New Post</a></li>
                                                        <%if(session.getAttribute("username")!=null && !session.getAttribute("username").toString().equals("")){%>
							<li><a title="blog" href="loginRegister.jsp">Logout(<%=session.getAttribute("username").toString()%>)</a></li>
                                                        <%}else {%>
                                                        <li><a title="blog" href="loginRegister.jsp">Login/Register</a></li>
                                                        <%}%>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<!-- Header area -->
	<div id="header-wrapper" class="header-slider">
		<header class="clearfix">
                        <%if (session.getAttribute("successMsg") != null && !session.getAttribute("successMsg").equals("")) {%>
                <div class="alert alert-success">
                    <%=session.getAttribute("successMsg")%>
                </div><%
                    session.setAttribute("successMsg", null);
                }%>
			<div class="logo">
				<img src="resources/img/logo-image.png" alt="" />
			</div>
			<div class="container">
				<div class="row">
					<div class="span12">
						<div id="main-flexslider" class="flexslider">
							<ul class="slides">
								<li>
									<p class="home-slide-content">
										<strong>Love</strong> and passion
									</p>
								</li>
								<li>
									<p class="home-slide-content">
										Beautiful <strong>nature</strong>
									</p>
								</li>
								<li>
									<p class="home-slide-content">
										We love <strong>traveling</strong>
									</p>
								</li>
							</ul>
						</div>
						<!-- end slider -->
					</div>
				</div>
			</div>
		</header>
	</div>
	<!-- spacer section -->
	<section class="spacer green">
		<div class="container">
			<div class="row">
				<div class="span6 alignright flyLeft">
					<blockquote class="large">
						There's huge space between creativity and imagination <cite>Mark Simmons, Nett Media</cite>
					</blockquote>
				</div>
				<div class="span6 aligncenter flyRight">
					<i class="icon-coffee icon-10x"></i>
				</div>
			</div>
		</div>
	</section>
	<!-- end spacer section -->
	
	<!-- section: blog -->
	<section id="blog" class="section">
		<div class="container">
			<h4>Travel Posts</h4>
			<!-- Three columns -->
			<div class="row">
                            <%
                                int count=0;
                                while(rs1.next()&& count++<6){%>
				<div class="span3">
					<div class="home-post">
						<div class="post-image">
							<img class="max-img" src="resources/img/coxs-bazar2.jpg" alt="" />
						</div>
						<div class="post-meta">
							<i class="icon-file icon-2x"></i>
							<span class="date"><%=rs1.getString("date")%></span>
						</div>
						<div class="entry-content">
							<h4><strong><a href="postDetails.jsp?id=<%=rs1.getString("id")%>"><%=rs1.getString("title")%></a></strong></h4>
							<h5><strong><a href="postDetails.jsp?id=<%=rs1.getString("id")%>"><%=rs1.getString("sub_title")%></a></strong></h5>
                                                        <p>
								<%=rs1.getString("content")%>
							</p>
                                                        <a href="postDetails.jsp?id=<%=rs1.getString("id")%>" class="more">Read more</a>
						</div>
					</div>
				</div>
                                <%}%>
			</div>
			<div class="blankdivider30"></div>
			<div class="aligncenter">
				<a href="posts.jsp" class="btn btn-large btn-theme">More posts</a>
			</div>
		</div>
	</section>


	<!-- end spacer section -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="span6 offset3">
					<p class="copyright">
						&copy; Maxim Theme. All rights reserved.
						<div class="credits">
							<!--
                All the links in the footer should remain intact.
                You can delete the links only if you purchased the pro version.
                Licensing information: https://bootstrapmade.com/license/
                Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Maxim
              -->
							<a href="https://bootstrapmade.com/">Free Bootstrap Templates</a> by BootstrapMade.com
						</div>
					</p>
				</div>
			</div>
		</div>
		<!-- ./container -->
	</footer>
	<a href="#" class="scrollup"><i class="icon-angle-up icon-square icon-bgdark icon-2x"></i></a>
	<script src="resources/js/jquery.js"></script>
	<script src="resources/js/jquery.scrollTo.js"></script>
	<script src="resources/js/jquery.nav.js"></script>
	<script src="resources/js/jquery.localScroll.js"></script>
	<script src="resources/js/bootstrap.js"></script>
	<script src="resources/js/jquery.prettyPhoto.js"></script>
	<script src="resources/js/isotope.js"></script>
	<script src="resources/js/jquery.flexslider.js"></script>
	<script src="resources/js/inview.js"></script>
	<script src="resources/js/animate.js"></script>
	<script src="resources/js/custom.js"></script>
	<script src="resources/contactform/contactform.js"></script>
<%
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                db.close();
            }
        %>
</body>

</html>
